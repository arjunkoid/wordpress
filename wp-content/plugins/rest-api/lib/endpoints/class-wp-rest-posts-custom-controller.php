<?php

add_action( 'rest_api_init', function () {
	register_rest_route( 'wp/v2', '/author', array(
		'methods' => 'GET',
		'callback' => 'my_awesome_func',
		'args' => array(
			'id' => array(
				'validate_callback' => 'is_numeric'
			),
		),
		'permission_callback' => function () {
			return true;
		}
	) );
} );

function my_awesome_func() {

	
	$wpdb = $GLOBALS['wpdb'];
	$data = $wpdb->get_results( "SELECT * FROM $wpdb->posts leftjoin" );
	$data[]['avatar_url'] = rest_get_avatar_urls('arjunkoid@gmail.com');
	$response = new WP_REST_Response( $data );
	return $response;
}