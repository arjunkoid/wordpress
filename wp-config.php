<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'wordpress_db');

/** MySQL database username */
define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', 'password');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'Tw&zm0eufm?Sa@g$K+o=_.`.H{g<]=L Y: tCNd1/vHCG-EnbJ}utO,S7>heYA#{');
define('SECURE_AUTH_KEY',  '$9W{r;!$i`|Ut23.up[3KJ.END`bN2e%]7V@g!x0emRgMsLk.fOI1FWD>@},k&/k');
define('LOGGED_IN_KEY',    '@AJt<7nk{v12Bs*sD.fEWV7l7}) 2+Lr9IMxevvgR[kbYkS}RcuHJQ7SX!.n(OTE');
define('NONCE_KEY',        '~N91=(,|?lD.YcD#z]`@=`A?0~hrM8)gZ,eT.1>+Z<]btVHYp%wtmEW|HJlAWAk@');
define('AUTH_SALT',        'oYL1_VPPSFyXk0[so}p?8X8[bk`lB/XAa]@))6Cqh(uME[fF,ez4,i>m}6,DZ&ss');
define('SECURE_AUTH_SALT', '4-B8iW24ksqFu#5un63@9`&Q>s/aOE%>bY$+X?- I2tUQ{4Qh21>=mV8CXQK-JyT');
define('LOGGED_IN_SALT',   'IR=Aw:(M=oJtOnz^{}h.F(BU<$Ea4wC[q(Qws}TPp^6U hw#K]wAZrg|~^K<H(B*');
define('NONCE_SALT',       ']1Ja3e_Vb1kIkxWc/qamwq9M2FtVkNb&7+<rIcUkH]Y@6JO_zA~Qczz>={d}.VW#');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
